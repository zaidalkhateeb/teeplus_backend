import { define } from 'typeorm-seeding';
import { Category } from '../entities/category.entity';

// @ts-ignore
define(Category, (faker: typeof  Faker) => {
  const firstName = faker.random.word()
  const category = new Category()
  category.name = firstName
  return category
})