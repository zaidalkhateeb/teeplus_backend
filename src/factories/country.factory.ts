import { define } from 'typeorm-seeding';
import { Country } from '../entities/country.entity';

// @ts-ignore
define(Country, (faker: typeof  Faker) => {
  const firstName = faker.address.country()
  const country = new Country()
  country.name = firstName
  return country
})