import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';


//gard for check access token


@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt')implements CanActivate{
  constructor(private readonly reflector: Reflector) {
    super();
  }
  public canActivate(context: ExecutionContext): any {
    const isPublic = this.reflector.get<boolean>('isPublic', context.getHandler());
    if (isPublic) {
      return true;
    }
    // Make sure to check the authorization, for now, just return false to have a difference between public routes.
    return super.canActivate(context);
  }
}
