import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import {env} from '../../env'


// jwt strategy for check accessToken is valid when user access to un public rout
// passport side
@Injectable()
export class jwtStrategy extends PassportStrategy(Strategy){
    constructor() {
      super({
        jwtFromRequest:ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: env.secretOrKey
      });
    }
  async validate(payload){
      return payload;
  }
}
