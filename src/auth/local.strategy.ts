import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthService } from './auth.service';
import { User } from '../entities/user.entity';



// this for check if user that logged in is exist or not
// passport side
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({ usernameField: 'email' });
  }

 async validate(email: string, password: string): Promise<any> {

    const user =await this.authService.validateUser(email, password);
    if (!user) {
     throw new UnauthorizedException();
    }
    return user;
  }
}
