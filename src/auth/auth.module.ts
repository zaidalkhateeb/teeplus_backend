import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersService } from '../users/users.service';
import { UsersModule } from '../users/users.module';
import { PassportModule, PassportStrategy } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { LocalAuthGuard } from './local-auth.guard';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { JwtAuthGuard } from './jwt-auth.guard';
import { jwtStrategy } from './jwt.strategy';
import { env } from '../../env';

@Module({
  imports:[UsersModule,PassportModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: env.secretOrKey,
    })],
  providers: [AuthService,LocalStrategy,LocalAuthGuard,jwtStrategy,JwtAuthGuard],
  exports:[AuthService],
  controllers: [AuthController]
})
export class AuthModule {}
