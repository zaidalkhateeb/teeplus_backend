import { Injectable, Request } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AuthService {

  constructor(private userService: UsersService, private jwtService: JwtService) {
  }

  async validateUser(email: string, password: string) {

    const user = await this.userService.findOne(email);

    if (user) {

      return await bcrypt.compare(password, user.password).then((res) => {
        const { password, ...payload } = user;
        if (res == true)
          return payload;
        else
          return null;
      });
    }
    return null;
  }

  async login(user) {
    const payload = {
      email: user.email,
      id: user.id,
    };
    return {
      access_token: this.jwtService.sign(payload),
      user: user,
    };
  }


  async signup(data) {
    const { password, ...user } = await this.userService.addUser(data);
    const payload = {
      email: user.email,
      id: user.id,
    };
    return {
      access_token: this.jwtService.sign(payload),
      user: user,
    };
  }


  async getAuthUser(@Request() request) {
    const payload:any = this.jwtService.decode(request.headers['authorization'].split(' ')[1]);
    return this.userService.findOneById(payload.id);;
  }
}
