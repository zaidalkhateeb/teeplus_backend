import { Body, Controller, Get, HttpStatus, Post, Request, Res, UseGuards } from '@nestjs/common';
import { LocalAuthGuard } from './local-auth.guard';
import { JwtService } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { Public } from '../public.decorator';
import { Response } from 'express';
import { UsersService } from '../users/users.service';
import { ExceptionsHandler } from '@nestjs/core/exceptions/exceptions-handler';
import { UserTypeEnum } from '../enums/UserType.enum';

@Controller('auth')
export class AuthController {
  constructor(private jwtService: JwtService, private authService: AuthService) {
  }

  @UseGuards(LocalAuthGuard)
  @Public()
  @Post('/login')
  async login(@Request() req, @Res() response: Response) {
    const data = await this.authService.login(req.user);
    return response.status(HttpStatus.OK).send(data);
  }

  @Post('/signup')
  @Public()
  async signup(@Body() body: any, @Res()response: Response) {
    if (body.type != UserTypeEnum.User && body.type != UserTypeEnum.Actor) {
      return response.status(HttpStatus.BAD_REQUEST).send({ message: 'User Type Should Be Actor Or User' });
    }
    const data = {
      data: await this.authService.signup(body),
      message: 'success',
    };
    return response.status(HttpStatus.OK).send(data);
  }


}
