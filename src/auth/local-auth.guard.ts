import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';


// for login
// we set local strategy so the local.strategy will firing
// if we have another strategy so we can build another AuthGard with another strategy like facebook strategy
@Injectable()
export class LocalAuthGuard extends AuthGuard('local'){

}
