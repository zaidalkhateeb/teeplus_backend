import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ArtWork } from '../entities/artWork.entity';
import { getConnection, Repository } from 'typeorm';
import { AuthService } from '../auth/auth.service';
import { Media } from '../entities/media.entity';
import { User } from '../entities/user.entity';

@Injectable()
export class ArtWorkService {

  constructor(@InjectRepository(ArtWork) private artWorkRepository: Repository<ArtWork>,
              @InjectRepository(Media) private mediaRepository: Repository<Media>) {
  }

  async getAllArtWorks() {
    return await this.artWorkRepository.createQueryBuilder('artWork')
      .innerJoinAndSelect('artWork.user', 'users')
      .innerJoinAndSelect('artWork.media', 'medias')
      .innerJoinAndSelect('artWork.category', 'categories')
      .getMany();
  }

  async addArtWork(user, body, artName) {
    try {
      const media: Media = await this.mediaRepository.save({ fileName: artName });
      const data = {
        mediaId: media.id,
        userId: user.id,
        categoryId: body.categoryId,
        height: body.height,
        width: body.width,
        backGroundColor: body.backGroundColor,
        title: body.title,
      };
      const artWork: ArtWork = await this.artWorkRepository.save(data);
      return artWork;
    } catch (e) {
      return new HttpException('ERROR', e);
    }
  }

  async getArtById(id): Promise<ArtWork | undefined| any> {
    try {
      const artWork = await this.artWorkRepository.findOne(id, {
        relations: ['media', 'user'],
      });
      return artWork;
    } catch (e) {
      return new HttpException('ERROR', e);
    }
  }

  async editArtWork(artWorkId,body){
    try {
       await this.artWorkRepository.update(artWorkId,body)
      return await this.artWorkRepository.findOne(artWorkId);
    }catch (e){
      return  new HttpException('ERROR', e);
    }

  }
}
