import { Module } from '@nestjs/common';
import { ArtWorkController } from './art-work.controller';
import { ArtWorkService } from './art-work.service';
import { ArtWork } from '../entities/artWork.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../entities/user.entity';
import { AuthService } from '../auth/auth.service';
import { AuthModule } from '../auth/auth.module';
import { Media } from '../entities/media.entity';
import { NestjsFormDataModule } from 'nestjs-form-data';

@Module({
  controllers: [ArtWorkController],
  providers: [ArtWorkService],
  imports:[TypeOrmModule.forFeature([ArtWork,Media]),AuthModule,NestjsFormDataModule],
  exports:[ArtWorkService]
})
export class ArtWorkModule {}
