import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post, Put, Query,
  Request,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { Public } from '../public.decorator';
import { ArtWorkService } from './art-work.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname, join } from 'path';
import { AuthService } from '../auth/auth.service';
import { Response } from 'express';
import { UserTypeEnum } from '../enums/UserType.enum';
import { ArtWork } from '../entities/artWork.entity';
import * as sharp from 'sharp';
import { Like } from 'typeorm';
import { FormDataRequest } from 'nestjs-form-data';
import * as fs from 'fs';
@Controller('artwork')
export class ArtWorkController {
  constructor(private artWorkService: ArtWorkService, private authService: AuthService) {
  }

  @Post()
  @UseInterceptors(FileInterceptor('image', {
    limits: { fileSize: 2010000 }, // 2 MP
    storage: diskStorage({
      destination: './uploads/media/arts/origin',
      filename: (req, file, cb) => {
        if (!file.originalname.match(/\.(png)$/)) {
          cb(new HttpException('Only png image is Accepted', HttpStatus.BAD_REQUEST));
        }
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16))
          .toString(16)).join('');
        return cb(null, `${randomName}${extname(file.originalname)}`);
      },
    }),
  }))
  async addArtWork(@Request() request, @Body() body: any, @UploadedFile() file, @Res()response: Response) {
    const authUser = await this.authService.getAuthUser(request);
    if (authUser.type != UserTypeEnum.Actor) {
      return response.status(HttpStatus.FORBIDDEN).send({ message: 'You Don\'t Have Permission To Do That' });
    }

    const data = {
      data: await this.artWorkService.addArtWork(authUser, body, file.filename),
      message: 'success',
    };
    // save lowe quality copy of file for public user and gust
    await sharp(join(process.cwd(), 'uploads/media/arts/origin/') + file.filename)
      .resize(700)
      .sharpen()
      .withMetadata()
      .webp({ quality: 90 })
      .toFile('public/media/arts/copy/' + file.filename);
    return response.status(HttpStatus.OK).send(data);
  }

  @Public()
  @Get('/')
  async getAllArtWorks(@Res()response: Response) {
    let data = await this.artWorkService.getAllArtWorks();

    const dataRes: [] = [];
    // @ts-ignore
    await data.map((art: ArtWork) => {
      art.media.fileName = '/media/arts/copy/' + art.media.fileName;
      // @ts-ignore
      let index = dataRes.findIndex(res => res.categoryName === art.category.name);
      if (index != -1) {
        // @ts-ignore
        dataRes[index].artsWork.push(art);
      } else {
        // @ts-ignore
        dataRes.push({ categoryName: art.category.name, artsWork: [art] });
      }
    });
    const res = {
      data: dataRes,
      message: 'success',
    };
    return response.status(HttpStatus.OK).send(res);
  }


  @Get('/:id')
  async getArtWork(@Request() request, @Param('id') id, @Res()response: Response) {
    const data: ArtWork = await this.artWorkService.getArtById(id);
    const authUser = await this.authService.getAuthUser(request);
    if (authUser.id != data.userId) {
      return response.status(HttpStatus.FORBIDDEN).send({ message: 'You Don\'t Have Permission To Do That' });
    }
    // @ts-ignore
    data.media.image = await this.getArtImageBase64WithoutWatermark(data.media.fileName);
    const res = {
      data: data,
      message: 'success',
    };
    return response.status(HttpStatus.OK).send(res);
  }

  @Get('/:id/view')
  @Public()
  // we return the copy file (low quality copy)
  async viewArtWork(@Param('id') id, @Res()response: Response) {
    const data: ArtWork = await this.artWorkService.getArtById(id);
    data.media.fileName = '/media/arts/copy/' + data.media.fileName;
    const res = {
      data: data,
      message: 'success',
    };
    return response.status(HttpStatus.OK).send(res);
  }

  @Get('/:id/download')
  @Public()
  async downloadArtWork(@Param('id') id, @Res()response: Response) {
    const data: ArtWork = await this.artWorkService.getArtById(id);
    const res = {
      data: { image: await this.getArtImageBase64WithWatermark(data.media.fileName, data.width,data.backGroundColor) },
      message: 'success',
    };
    return response.status(HttpStatus.OK).send(res);
  }

  @Put('/:id')
  @FormDataRequest() // for get body from data form
  async editArtWOrk(@Param('id') id, @Body() body, @Request() request, @Res() response: Response) {
    const authUser = await this.authService.getAuthUser(request);
    const artWork: ArtWork = await this.artWorkService.getArtById(id);
    if (authUser.id != artWork.userId) {
      return response.status(HttpStatus.FORBIDDEN).send({ message: 'You Don\'t Have Permission To Do That' });
    }
    const data = {
      data: await this.artWorkService.editArtWork(id, body),
      message: 'success',
    };
    return response.status(HttpStatus.OK).send(data);
  }

  // convert image file to base64 without watermark it is just for the owner of art
  // make sure to un save new file after convert if save the size of project still growing (so we return file as base64)
  async getArtImageBase64WithoutWatermark(image: string) {
    // await sharp(join(process.cwd(), 'watermark.png'))
    //   .resize(500)
    //   .toFile('watermarkz.png');

    const data = await sharp(join(process.cwd(), 'uploads/media/arts/origin/') + image)
      .toBuffer();
    return `data:image/png;base64,${data.toString('base64')}`;

  }

  // convert image file to base64 without watermark it is user and gust
  // make sure to un save new file after convert if save the size of project still growing (so we return file as base64)
  async getArtImageBase64WithWatermark(image: string, size: string,color:string) {
   let imageColorName=''
   if(color==='black'){
     imageColorName='blackImage.png';
   }else{
     imageColorName='whiteImage.png';
   }


   // if size 100% just send default file without background because size is 100% so background not appear
    if(size==='100%'){

      await sharp(join(process.cwd(), 'watermark.png'))
        .resize(850)
        .toFile('./watermarkCopy.png');
      const data =  await sharp(join(process.cwd(), 'uploads/media/arts/origin/') + image)
        .resize(900)
        .composite([{ input: './watermarkCopy.png', gravity: 'center', blend: 'over' }])
        .toBuffer();


      fs.unlinkSync('temp.png')
      fs.unlinkSync('watermarkCopy.png')
      return `data:image/png;base64,${data.toString('base64')}`;
    }else{
      // if size 50% make art and watermark half (450) of normal size (900)
      // then add background with color as user choose in normal size (900)
      // then put the old art on it
      console.log(image);
      await sharp(join(process.cwd(), 'watermark.png'))
        .resize(400)
        .toFile('./watermarkCopySmall.png');

      await sharp(join(process.cwd(), 'uploads/media/arts/origin/') + image)
        .resize(450)
        .composite([{ input: './watermarkCopySmall.png', gravity: 'center', blend: 'over' }])
        .toFile(image);

      const res =  await sharp(join(process.cwd(), imageColorName))
        .resize(900)
        .composite([{ input: image, gravity: 'center', blend: 'over' }])
        .toBuffer()

      fs.unlinkSync(image)
      fs.unlinkSync('./watermarkCopySmall.png')
      return `data:image/png;base64,${res.toString('base64')}`;
    }

  }

}
