import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from '../entities/category.entity';
import { Country } from '../entities/country.entity';

@Module({
  controllers: [AdminController],
  providers: [AdminService],
  imports:[TypeOrmModule.forFeature([Category,Country])]
})
export class AdminModule {}
