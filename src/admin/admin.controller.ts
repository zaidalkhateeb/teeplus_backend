import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import { AdminService } from './admin.service';
import { Public } from '../public.decorator';
import { Response } from 'express';

@Controller('admin')
export class AdminController {
  constructor(private adminService:AdminService) {
  }

  @Get('categories')
  @Public()
  async getAllCategories(@Res() response:Response){
    const data={
      data:await this.adminService.getAllCategories(),
      message:'success'
    }
   return response.status(HttpStatus.OK).send(data);
  }


  @Get('countries')
  @Public()
  async getAllCountries(@Res() response:Response){
    const data={
      data:await this.adminService.getAllCountries(),
      message:'success'
    }
    return response.status(HttpStatus.OK).send(data);
  }
}
