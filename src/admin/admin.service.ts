import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Category } from '../entities/category.entity';
import { Country } from '../entities/country.entity';

@Injectable()
export class AdminService {
  constructor(@InjectRepository(Category) private categoryRepository: Repository<Category>,
              @InjectRepository(Country) private countryRepository: Repository<Country>) {
  }

  async getAllCategories() {
    return await this.categoryRepository.find();
  }
  async getAllCountries(){
    return await this.countryRepository.find();
  }
}
