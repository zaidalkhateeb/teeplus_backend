import { Injectable } from '@nestjs/common';
import { User } from '../entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User)
              private usersRepository: Repository<User>) {
  }

  async findOne(email: string): Promise<User | undefined> {
    return await this.usersRepository.createQueryBuilder('user').where('email = :email').setParameters({ email: email }).getOne();
  }
  async findOneById(id: number): Promise< any | undefined> {
    const { password, ...user }: User = await this.usersRepository.findOne(id);
    return user;
  }

  async addUser(data) {
    data.password = await bcrypt.hash(data.password, 10);
    return this.usersRepository.save(data);
  }

  async getAuthUser(request) {
    // const { iat, exp, ...user } = this.jwtService.decode(request.headers['authorization'].split(' ')[1]);
    // return user;
  }
}
