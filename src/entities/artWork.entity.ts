import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Category } from './category.entity';
import { Media } from './media.entity';
import { User } from './user.entity';

@Entity('art_works')
export class ArtWork{
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: string;
  @Column()
  backGroundColor: string;
  @Column()
  height: string;
  @Column()
  width: string;
  @Column()
  categoryId: number;
  @Column()
  mediaId: number;
  @Column()
  userId: number;

  @ManyToOne(()=>Category,Category=>Category.artWorks)
  category:Category

  @OneToOne(()=>Media,Media=>Media.artWork)
  @JoinColumn()
  media:Media

  @ManyToOne(()=>User,User=>User.artWorks)
  user:User;

}