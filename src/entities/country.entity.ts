import { Column, Entity, JoinTable, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';


@Entity('countries')
export class Country{
  @PrimaryGeneratedColumn()
  id:number;

  @Column()
  name:string

  @OneToMany(() => User,User=>User.country)
  users:User[];
}