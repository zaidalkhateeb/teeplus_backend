import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ArtWork } from './artWork.entity';
@Entity('categories')
export class Category {
  @PrimaryGeneratedColumn()
  id:number;
  @Column()
  name:string;
  @OneToMany(() => ArtWork, ArtWork => ArtWork.category)
  artWorks: ArtWork[];
}