import { Column, Entity, JoinTable, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, Repository } from 'typeorm';
import { Country } from './country.entity';
import { ArtWork } from './artWork.entity';
import { classToPlain, Exclude } from 'class-transformer';

@Entity('users')
export class User {

  @PrimaryGeneratedColumn()
  id: number;
  @Column({ unique: true })
  email: string;
  @Column()
  name: string;
  @Column()
  @Exclude({ toPlainOnly: true })
  password: string;
  @Column()
  countryId: number;
  @Column()
  type: number;
  @ManyToOne(() => Country, Country => Country.users, { nullable: false })
  country: Country;
  @OneToMany(()=>ArtWork,ArtWork=>ArtWork.user)
  artWorks:ArtWork[];

  toJSON() {
    return classToPlain(this);
  }
}

