import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ArtWork } from './artWork.entity';

@Entity('medias')
export class Media {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  fileName:string;
  @OneToOne(() => ArtWork, ArtWork => ArtWork.category)
  artWork: ArtWork;
}