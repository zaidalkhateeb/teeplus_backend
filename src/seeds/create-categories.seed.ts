import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Category } from '../entities/category.entity';

export default class createCategories implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await factory(Category)().createMany(20)
  }
}