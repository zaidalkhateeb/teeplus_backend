import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Country } from '../entities/country.entity';

export default class createCountries implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await factory(Country)().createMany(120)
  }
}