import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule,{ cors:{allowedHeaders:'*'} });
  app.setGlobalPrefix('api');
  await app.listen(3000);
//  app.useGlobalGuards(new AuthGuardGlobal(app))

}

bootstrap();
